#!/bin/bash

# Install bashrc, ssh keys and vim just the way I like them

set -e

function main() {
    add_to_bashrc
    setup_ssh_keys
    setup_vim
    setup_asdf
}

function add_to_bashrc() {
    cat << 'EOF' >> ~/.bash_profile
for SCRIPT in ~/.dotfiles/bash/*; do
    [[ -s $SCRIPT ]] && source $SCRIPT
done
EOF
}

function setup_ssh_keys() {
    [ -d "~/.ssh" ] && mv ~/.ssh ~/.ssh.bak
    ln -s ~/.dotfiles/ssh ~/.ssh
    chmod og-rwx ~/.ssh/*
}

function setup_vim() {
    setup_vimrc && install_vim_plugins
}

function setup_vimrc() {
    [[ -e ~/.vimrc ]] && mv ~/.vimrc ~/.vimrc.bak
    ln -s ~/.dotfiles/vim/vimrc ~/.vimrc
}

function install_vim_plugins() {
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    echo "Remember to run :PlugInstall"
}

function setup_asdf() {
    [ -d ~/.asdf ] || git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.2.1
}

main
