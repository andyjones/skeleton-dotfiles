.dotfiles
=========

This is a skeleton to demonstrate how I currently setup my .dotfiles and vim
config.

    git clone git@gitlab.com:andyjones/skeleton-dotfiles.git ~/.dotfiles
    .dotfiles/bootstrap.sh

Highlights
--------

 * version controlled dot files
 * shared ssh keys
 * work in `~/work`
 * go root in `~/play/go`
 * timestamp bash history and keep lots of it
 * installs ASDF
 * vim: custom vimrc, Plug plugin manager, store .swp files in ~/tmp or /tmp

./bin
-----

I add local scripts and programs here. I don't version control them (yet)
because I don't have many.

Currently it contains:

 * [backspace](https://github.com/WeSwapTech/backspace/) to upload/download files to Rackspace
 * [code-owners](https://gist.github.com/andyjones/c7fe637dc1006759f1558584ed0ec199)
 * [git-cloneall](https://gist.github.com/andyjones/667f053384e93b8fd6eb1de1ebc6f245) to clone all repos from an organisation
 * [git-pullall](https://gist.github.com/andyjones/453b16177a9910ff625d6edcaa9eb456) to pull across checked out repos
 * [git-sweep](https://gist.github.com/andyjones/db2031807e902491f43553ef7a3f0048) to delete merged local branches
 * [hub](https://github.com/github/hub) for raising PRs from the command line
 * [rg](https://github.com/BurntSushi/ripgrep) for grepping across repos

Alternative approaches that didn't work for me
----------------------------------------------

 * home directory under git (too many untracked files etc)
